def diplomacy_read(s):
    """
    read army, location, action [,destination or supporting army]

    if action is hold, return [army, initial location, [hold, ""], initial location]
    if action is move, return [army, initial location, [hold, destination], initial location]
    if action is support, return [army, initial location, [support, supporting army], initial location]
    """

    a = s.split()
    if len(a) == 3:
        return [str(a[0]), str(a[1]), [str(a[2]), ""], str(a[1])]
    else:
        return [str(a[0]), str(a[1]), [str(a[2]), str(a[3])], str(a[1])]

def diplomacy_eval(situation):
    """
    input: a 2d list consisting of each army, location, action
        as well as optional paramter based on action their action
    output: army name followed by destination of it survives or dead otherwise

    """

    attack_size = []
    attacked_by = []

    # initialize lists that keep track armys that have been attacked,
    # and size of army with supporting armys

    # pre-conditions
    assert situation
    assert len(situation[0]) == 4

    for i in range(len(situation)):
        attack_size.append("")
        attacked_by.append("N")

    for i in range(len(situation)):
        # add army to the index of the army it supports in attacked size
        if situation[i][2][0] == "Support":
            army_val = ord(situation[i][2][1]) % 65
            attack_size[army_val] = attack_size[army_val] + situation[i][0]

        # update location of army that moves to where it is moving
        elif situation[i][2][0] == "Move":
            situation[i][1] = situation[i][2][1]
            for j in range(len(situation)):
                # if a moving armies location is another armies initial location,
                # update attacked_by of the other army to show it was attacked
                if (i != j) and situation[i][1] == situation[j][3]:
                    attacked_by[j] = 'Y'

    # if an army has been attacked, remove all support it has offered
    for i in range(len(attacked_by)):
        if attacked_by[i] == "Y":
            for j in range(len(attack_size)):
                if situation[i][0] in attack_size[j] and i != j:
                    attack_size[j] = attack_size[j].replace(situation[i][0], "")

    # create a list that has each army's current location, organized by index
    updated_board = []
    for i in range(len(attacked_by)):
        updated_board.append(situation[i][1])

    for i in range(len(attacked_by)):
        # if army has been attacked or is attacking
        if attacked_by[i] == "Y" or situation[i][2][0] == "Move":
            for j in range(len(attacked_by)):
                # evaluate outcome for armies that fight each other
                if (i != j) and (situation[j][2][0] == "Move") and (situation[j][2][1] == updated_board[i]):
                    if len(attack_size[i]) > len(attack_size[j]):
                        situation[j][1] = "[dead]"
                    elif len(attack_size[j]) > len(attack_size[i]):
                            situation[i][1] = "[dead]"
                    else:
                        situation[i][1] = "[dead]"
                        situation[j][1] = "[dead]"

    # return 2d list of each army and its final destination if it survives, and [dead] otherwise
    final_board = []
    for n in range(len(situation)):
        final_board.append([situation[n][0], situation[n][1]])

    # post-conditions
    assert final_board
    assert len(final_board) ==len(situation)

    return final_board

def diplomacy_print(w, i):
    """
    w is a writer
    i is a list consisting of lists of each army and their final location

    """
    for j in i:
        w.write(str(j[0]) + " " + str(j[1]) + "\n")

def diplomacy_solve(r,w):
    """
    r a reader
    w a writer
    """
    cur_situation = []
    for s in r:
        cur_situation.append(diplomacy_read(s.replace("\n","")))
    cur_situation.sort()
    i = diplomacy_eval(cur_situation)
    diplomacy_print(w, i)
